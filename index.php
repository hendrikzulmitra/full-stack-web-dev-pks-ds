<?php

trait Hewan {

public $nama ;
public $darah;
public $jumlahKaki;
public $keahlian;

public function profil($nama, $jumlahKaki, $keahlian, $darah = 50){
    $this -> nama = $nama ;
    $this -> darah = $darah ;
    $this -> jumlahKaki = $jumlahKaki ;
    $this -> keahlian = $keahlian ;
}

public function atraksi($nama, $keahlian){
    return $this->nama . "sedang " . $this->keahlian ;
}

}

trait Fight {
    public $attackPower ;
    public $defencePower ;

    public function status($attackPower, $defencePower){
        $this -> attackPower = $attackPower;
        $this -> defencePower = $defencePower;

    }
}

class Elang {
    use Hewan, Fight;
    public function tampilkan($jumlahKaki, $keahlian, $attackPower, $defencePower){
        return $this->jumlahKaki . $this->keahlian . $this->attackPower . $this->defencePower;
    }
}

$elang = new Elang();
echo $elang -> tampilkan(2, 'terbang tinggi', 10, 5);
echo $elang-> atraksi ('elang', 'terbang tinggi');





class Harimau {
    use Hewan, Fight;
    public function tampilkan($jumlahKaki, $keahlian, $attackPower, $defencePower){
        return $this->jumlahKaki . $this->keahlian . $this->attackPower . $this->defencePower;
    }
}

$harimau = new Harimau();
echo $harimau -> tampilkan(2, 'lari cepat', 7, 8);
echo $harimau-> atraksi ('harimau', 'lari cepat');

?>